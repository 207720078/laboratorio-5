''''
#Haciendo uso de un ciclo anidado FOR simule que: en cada una de las
#entidades, cada persona realiza un pago de cada servicio.

lista = list()

entidad = ["BN Popular", "BN Nacional", "BN Central de Costa Rica"]
persona = ["Juan", "Alberto", "Yessenia"]
servicio = ["Servicios Municipales", "Pago de Luz", "Pago Agua"]

for i in entidad:
    for j in persona:
        for y in servicio:
            print("Deposito en el",i,"de" ,j, "por concepto de", y)
'''


''''
#Haciendo uso de un ciclo WHILE, realice un menú que tenga 3 Opciones 
#principales y cada una de estas opciones tenga dos opciones internas. No 
#es necesario realizar ninguna acción, únicamente demostrar el 
#comportamiento de los ciclos correctamente aplicados en un menú.

def pedirNumeroEntero():

    correcto=False
    num=0
    while(not correcto):
        try:
            num = int(input("Introduce un numero entero: "))
            correcto=True
        except ValueError:
            print('Error, introduce un numero entero')

    return num

salir = False
opcion = 0

while not salir:

    print ("1. Opcion 1")
    print ("2. Opcion 2")
    print ("3. Opcion 3")
    print ("4. Salir")

    print ("Elige una opcion")

    opcion = pedirNumeroEntero()

    if opcion == 1:
        print ("Opcion 1")
    elif opcion == 2:
        print ("Opcion 2")
    elif opcion == 3:
        print("Opcion 3")
    elif opcion == 4:
        salir = True
    else:
        print ("Introduce un numero entre 1 y 3")

print ("Fin")
'''


''''
#Desarrolle un ejercicio programado, donde ejemplifique el uso de la
#herencia, el ejemplo no puede ser los mismos vistos en clase, debe ser
#de autoría del estudiante. Este ejemplo debe tener las clases, la
#instancia y el consumo de los atributos y métodos de la clase de la
#cual hereda.


class Humano:
    def __init__(self, p_edad):
        self.edad = p_edad

    def hablar(self, comentario):
        print(comentario)

class Veterinario(Humano):
    def operar(self, animal):
        print("Voy a operar a una", animal)

richard = Veterinario(21)

richard.hablar("Hola")
richard.operar("Vaca")
'''


''''
#El siguiente ejemplo de código contiene errores corríjalos para que el
#ejemplo se ejecute de manera correcta, indique en comentarios cuales
#fueron los errores encontrados.


class Articulo:
    def __init__(self, p_codigo, p_nombre):
        self.codigo = p_codigo
        self.nombre = p_nombre

    def getNombre(self):
        return self.nombre

    def getColor(self):
        return self.codigo


class Juguete(Articulo):
    def __init__(self, codigo, nombre,precio):
        super().__init__(codigo, nombre)
        self.__precio = precio


    def getDescripcion(self):
        return self.getNombre() + self.__precio + " de color" + self.getColor()

objeto = Juguete("10", "Carro", "5000")

print(objeto.getDescripcion())
print(objeto.getNombre())


#Hacía falta la clase padre Articulos de donde la clase Juguete heredó atributos
#Hacía falta instanciar la clase Juguete
#Hacía falta crear los métodos para "getNombre" y "getColor"
'''''
